var validation = {
    kiemTraRong: function (value, idError, message){
        if (value.length == 0){
            document.getElementById(idError).innerText = message;
            return false;
        } else {
            document.getElementById(idError).innerText= "";
            return true;
        }
    },
    kiemTraDoDai: function(value, idError, message, min, max)
    {
        if(value.length < min || value.length > max){
            document.getElementById(idError).innerText = message;
        } else {
            document.getElementById(idError).innerText= "";
            return true;
        }
    },
    kiemTraEmail: function(value, idError, message){
        const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        // var gmailTest = "Alice@gmail.com";
        // var isGmail = regex.test(gmailTest);
        // console.log('isGmail: ', isGmail);
        if (re.test(value)){
            document.getElementById(idError).innerText= "";
            return true;
        }else{
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    // kiemTraDoManhMatKhau: function(value, idError, message){
    //     var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    //     // var gmailTest = "Alice@gmail.com";
    //     // var isGmail = regex.test(gmailTest);
    //     // console.log('isGmail: ', isGmail);
    //     if (strongRegex.test(value)){
    //         document.getElementById(idError).innerText= "";
    //         return true;
    //     }else{
    //         document.getElementById(idError).innerText = message;
    //         return false;
    //     }
    // },
};