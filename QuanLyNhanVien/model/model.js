export class quanLiNhanVien{ 
    constructor(taiKhoan, ten, email, password, ngayLam, luong, chucVu, gioLam){
    this.taiKhoan = taiKhoan;
    this.ten = ten;
    this.email = email;
    this.password = password;
    this.ngayLam = ngayLam;
    this.luong = luong;
    this.chucVu = chucVu;
    this.gioLam = gioLam;
    this.tinhDTB = function(){
        return (this.diemToan + this.diemLy + this.diemHoa) / 3;
    }
}
}