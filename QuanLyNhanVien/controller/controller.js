import { quanLiNhanVien} from "../controller/controller.js";
export function layThongTinTuForm(){
    const taiKhoan= document.getElementById("tknv").value;
    const ten= document.getElementById("name").value;
    const email= document.getElementById("email").value;
    const password= document.getElementById("password").value;
    const ngayLam= document.getElementById("datepicker").value;
    const luong= document.getElementById("luongCB").value;
    const chucVu= document.getElementById("chucvu").value;
    const gioLam= document.getElementById("gioLam").value;

    return new nhanVien(taiKhoan, ten, email, password, ngayLam, luong, chucVu, gioLam);
}

export function renderDSNV(list){
    let contentHTML = "";
    list.forEach((item) =>
    {
        let contentTr = /*html*/ `<tr>
        <td>${item.taiKhoan}</td>
        <td>${item.ten}</td>
        <td>${item.email}</td>
        <td>${item.password}</td>
        <td>${item.ngayLam}</td>
        <td>${item.luong}</td>
        <td>${item.chucVu}</td>
        <td>${item.gioLam}</td>
        <td>
            <button onclick="xoaMonAn('${item.id}')" class="btn btn-danger">Xóa</button>
            <button class="btn btn-warning">Sửa</button>
            </td>
            </tr>
        `;
        contentHTML += contentTr;
    })
    document.getElementById("tbodyFood").innerHTML = contentHTML;
}
