const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";
//chức năng thêm sinh viên, tạo ngoài hàm vì nếu tạo trong in ra thì the end

var dssv = [];
// láy thông tin từ localStorage
var dsnvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
console.log("dsnvJson: ", dsnvJson);
if (dsnvJson != null) {
  console.log("Yes");
  dssv = JSON.parse(dsnvJson);
  // array khi convert thanh json se mat function
  for (index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];

    dsnv[index] = new sinhVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.password,
      nv.ngayLam,
      nv.luong,
      nv.chucVu,
      nv.giioLam
    );
  }
}
function themNV() {
  var newNv = layThongTinTuForm();
  // console.log('newSV: ', newSV);
  var isValid =
    validation.kiemTraRong(
      newNv.taiKhoan,
      "tbTKNV",
      "Tài khoản không được rỗng"
    ) &&
    // validation.kiemTraRong(
    //   newSv.maSV,
    //   "spanMaSV",
    //   "Mã sinh viên không được rỗng"
    // ) &
    // validation.kiemTraRong(
    //   newSv.email,
    //   "spanEmailSV",
    //   "Email sinh viên không được rỗng"
    // ) &
    // validation.kiemTraRong(
    //   newSv.password,
    //   "spanMatKhau",
    //   "Mật khẩu sinh viên không được rỗng"
    // ) &
    validation.kiemTraDoDai(
      newNv.taiKhoan,
      "tbTKNV",
      "Mã sinh viên phải có 6 kí tự",
      6,
      6
    );
  isValid =
    validation.kiemTraRong(
      newSv.ten,
      "tbTen",
      "Tên sinh viên không được rỗng"
    ) &&
    validation.kiemTraDoDai(
      newSv.ten,
      "tbTen",
      "Tên sinh viên phải có 4 kí tự",
      4,
      4
    );
  isValid =
    validation.kiemTraRong(
      newSv.email,
      "tbEmail",
      "Email không được rỗng"
    ) &&
    validation.kiemTraEmail(
      newSv.email,
      "tbEmail",
      "Email phải có cấu trúc abc@gmai.com"
    );
  isValid = validation.kiemTraRong(
    newSv.password,
    "spanMatKhau",
    "Mật khẩu không được rỗng"
  ); 
  isValid = validation.kiemTraRong(
    newSv.ngayLam,
    "spanMatKhau",
    "Ngày làm không được rỗng"
  );
  // validation.kiemTraDoDai(
  //   newSv.password,
  //   "spanMatKhau",
  //   "Mật khẩu phải có ít nhất 8 kí tự",
  //   8,
  //   8
  // );
  if (isValid) {
    dsnv.push(newNv);
    // Create JSON
    var dsnvJson = JSON.stringify(dsnv);
    // save json into localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dsnvJson);
    renderDSSV(dssv);
    // console.log('dssv: ', dssv);
  }
}

function deleteSinhVien(id) {
  // id được truyền vào từ "deleteSinhvien(${sv.maSV})"
  console.log(id);

  // var index = dssv.findIndex(function(sv){
  //     return sv.maSV == id;
  // });
  var index = timKiemViTri(id, dssv);
  // console.log(index);
  if (index !== -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index !== -(-1)) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
function resetSinhVien() {
  document.getElementById("txtMaSV").reset();
  document.getElementById("txtTenSV").reset();
  document.getElementById("txtEmail").reset();
  document.getElementById("txtPass").reset();

}
